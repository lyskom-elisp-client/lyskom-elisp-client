(def-kom-var kom-builtin-server-aliases
  '(("com.lysator.liu.se" . "LysCOM (LysKOM in English)"))
  "**An alist mapping server names to shorter identification strings.

Each value in this string should be of the form (`SERVER' . `NICKNAME'),
where `NICKNAME' is the short name for the server `SERVER'. Avoid
setting this variable since that will override the list compiled into
the client. Use `kom-server-aliases' instead.

Values other than those described are reserved for future use.")
